import React, {useContext, useState} from 'react';
import Note from '../../components/Note/Note';
import Grid from '@material-ui/core/Grid';
import {AuthContext} from '../../contexts/auth/authContext';
import Alert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import {useStyles} from './styles';
import {NoteContext} from '../../contexts/notes/noteContext';
import Loader from '../../components/Loader/Loader';
import AppConfirm from '../../components/AppConfirm/AppConfirm';
import {ConfirmContext} from '../../contexts/confirm/confirmContext';
import AppUserMenu from '../../components/AppUserMenu/AppUserMenu';

const Home = props => {
    const {user} = useContext(AuthContext);
    const {notes, deleteNote} = useContext(NoteContext);
    const confirm = useContext(ConfirmContext);
    const [deletedNote, setDeletedNote] = useState();
    const classes = useStyles();

    const deleteNoteHandler = (anchor, noteId) => {
        confirm.show(anchor);
        setDeletedNote(noteId);
    };

    function renderHome() {
        if (user) {
            if (!notes) {
                return (
                    <Loader/>
                )
            } else {
                return (
                    <React.Fragment>
                        <Typography variant="caption" display="block" gutterBottom paragraph className={classes.notesCount}>
                            Всего заметок: {notes.length}
                        </Typography>
                        {notes.length ?
                            <Grid container spacing={3}>
                                {notes.map((note, index) => {
                                    return (
                                        <Grid item xs={4} key={index}>
                                            <Note title={note.theme} text={note.note} id={note.id} onDelete={deleteNoteHandler}/>
                                        </Grid>
                                    )
                                })}
                            </Grid> : <Alert severity="info">Заметок не найдено</Alert>}
                        <AppConfirm onConfirm={() => deleteNote(deletedNote)}/>
                        <AppUserMenu/>
                    </React.Fragment>
                )
            }

        } else {
            return (
                <Alert severity="info">Войдите в систему для того чтобы оставлять заметки</Alert>
            )
        }
    }

    return (
        <React.Fragment>
            { renderHome() }
        </React.Fragment>

    );
};

export default Home;