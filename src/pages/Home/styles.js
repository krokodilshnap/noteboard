import {makeStyles} from '@material-ui/core/styles';
import {grey} from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
    notesCount: {
        color: grey[500]
    }
}))