import React, {useContext, useEffect, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import {AddNoteBtn} from './styles';
import {AuthContext} from '../../contexts/auth/authContext';
import axios from 'axios';
import {validateField} from '../../helpers/validation';
import {SnackbarContext} from '../../contexts/snackbar/snackbarContext';
import {useLocation, useParams, useHistory} from 'react-router-dom';

const CreateNote = props => {
    const [theme, setTheme] = useState('');
    const [note, setNote] = useState('');
    const [editMode, setEditMode] = useState(false);
    const {user} = useContext(AuthContext);
    const [valid, setValid] = useState(true);
    const snackbar = useContext(SnackbarContext);
    const location = useLocation();
    const history = useHistory();
    const {id} = useParams();

    const createNoteHandler = async (event) => {
        try {
            if (!validateField(note)) {
                setValid(false);
                return false;
            } else {
                await axios.post(`${process.env.REACT_APP_API_URL}/notes.json`, {
                    theme, note, author: user.email
                });
                clearForm();
                setValid(true);
                snackbar.showWithMessage('Заметка добавлена', 'success');
            }
        } catch (e) {
            console.log(e);
        }
    };

    const updateNoteHandler = async () => {
        try {
            await axios.patch(`${process.env.REACT_APP_API_URL}/notes/${id}.json`,  {note, theme});
            snackbar.showWithMessage('Заметка изменена', 'success');
            history.push('/');
        } catch (e) {
            snackbar.showWithMessage('Ошибка редактирования', 'error');
        }

    };

    useEffect(() => {
        if (location.pathname.includes('/edit')) {
            setEditMode(true);
            setEditNote().then(response => {
                const {note, theme} = response.data;
                setTheme(theme);
                setNote(note);
            });
        }
    }, [location]);

    const setEditNote = async () => {
        const response = await axios.get(`${process.env.REACT_APP_API_URL}/notes/${id}.json`);
        return response;
    };

    const clearForm = () => {
        setTheme('');
        setNote('');
    };

    return (
        <div>
            <h1>{editMode ? 'Редактировать' : 'Создать' } заметку</h1>
            <div>
                <TextField
                    margin="normal"
                    id="theme"
                    label="Тема заметки"
                    size="small"
                    value={theme}
                    onChange={(event) => setTheme(event.target.value)}
                    fullWidth
                    variant="outlined"
                />
            </div>
           <div>
               <TextField
                   margin="normal"
                   id="text"
                   required
                   error={!valid}
                   fullWidth
                   label="Заметка"
                   size="small"
                   value={note}
                   onChange={(event) => setNote(event.target.value)}
                   variant="outlined"
                   multiline
               />
           </div>
            <AddNoteBtn variant="contained" color="primary" onClick={editMode ? updateNoteHandler : createNoteHandler}>{editMode ? 'Сохранить' : 'Создать' }</AddNoteBtn>
        </div>
    );
};

export default CreateNote;