import {Button, withStyles} from '@material-ui/core';
import {green} from '@material-ui/core/colors';

export const AddNoteBtn = withStyles({
    root: {
        boxShadow: 'none',
        backgroundColor: green[500],
        marginTop: 10
    }
})(Button);