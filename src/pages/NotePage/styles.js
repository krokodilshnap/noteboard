import {makeStyles} from '@material-ui/core/styles';
import {indigo} from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
    editBtn: {
        marginRight: 10,
        color: indigo[700]
    },
    editBtnIcon: {
        marginRight: 5
    },
    noteActions: {
        display: 'flex',
        marginBottom: '30px',
        justifyContent: 'space-between'
    }
}));