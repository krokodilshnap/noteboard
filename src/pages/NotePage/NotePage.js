import React, {useContext, useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import {NoteContext} from '../../contexts/notes/noteContext';
import {useStyles} from './styles';
import EditIcon from '@material-ui/icons/Edit';
import {SnackbarContext} from '../../contexts/snackbar/snackbarContext';
import {ConfirmContext} from '../../contexts/confirm/confirmContext';
import AppConfirm from '../../components/AppConfirm/AppConfirm';
import {NavLink, useParams} from 'react-router-dom';
import Loader from '../../components/Loader/Loader';

const NotePage = props => {
    const [theme, setTheme] = useState();
    const [note, setNote] = useState();
    const {deleteNote} = useContext(NoteContext);
    const confirm = useContext(ConfirmContext);
    const snackbar = useContext(SnackbarContext);
    const classes = useStyles();
    const {id} = useParams();

    function fetchNote(id) {
       return axios.get(`${process.env.REACT_APP_API_URL}/notes/${id}.json`)
    }

    useEffect(() => {
        fetchNote(id).then((response) => {
            setTheme(response.data.theme);
            setNote(response.data.note);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const confirmDeleteHandler = () => {
        deleteNote(id);
        setTimeout(() => {
            props.history.push('/');
        }, 1000);
        snackbar.showWithMessage('Заметка удалена', 'success');
    };

    return (
        <React.Fragment>
        { note ?
                <div>
                <div className={classes.noteActions}>
                    <Button component={NavLink} to={'/edit/' + id} size="small" color={'default'} disableElevation variant="contained" className={classes.editBtn}>
                        <EditIcon fontSize={'small'} className={classes.editBtnIcon}/>Редактировать</Button>
                    <Button onClick={(event) => confirm.show(event.currentTarget)} size="small" color={'secondary'}>Удалить</Button>
                    <AppConfirm onConfirm={confirmDeleteHandler}/>
                </div>
                <Typography variant="h2">
                    <strong>{theme}</strong>
                </Typography>
                <Typography>
                    {note}
                </Typography>
            </div> :
                <Loader/> }
        </React.Fragment>

    );
};

export default NotePage;