import React, {useContext} from 'react';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import {ConfirmContext} from '../../contexts/confirm/confirmContext';
import {useStyles} from './styles';

const AppConfirm = props => {
    const {opened, setOpened, anchor, onConfirm, onDecline} = useContext(ConfirmContext);
    const classes = useStyles();
    const onConfirmHandler = () => {
        onConfirm();
        if (props.onConfirm) props.onConfirm();

    };

    const onDeclineHandler = () => {
        onDecline();
        if (props.onDecline) props.onDecline();
    };

    return (
        <Popover
            open={opened}
            anchorEl={anchor}
            onClose={() => setOpened(false)}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
        >
            <div className={classes.content}>
                Действительно хотите удалить?
                <div className={classes.btns}>
                    <Button size="small" color={'secondary'} disableElevation variant="contained" onClick={onConfirmHandler}>Да</Button>
                    <Button size="small" color={'default'} disableElevation variant="contained" onClick={onDeclineHandler}>Нет</Button>
                </div>
            </div>
        </Popover>
    );
};

export default AppConfirm;