import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    content: {
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    btns: {
        display: 'grid',
        gridTemplateColumns: 'auto auto',
        gridGap: '10px',
        marginTop: '10px'
    }
}));