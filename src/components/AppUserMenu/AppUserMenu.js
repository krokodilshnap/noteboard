import React, {useContext} from 'react';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import {AuthContext} from '../../contexts/auth/authContext';
import {UserMenuContext} from '../../contexts/userMenu/userMenuContext';
import {useStyles} from './styles';
import {useHistory} from 'react-router-dom';

const AppUserMenu = props => {
    const {user} = useContext(AuthContext);
    const history = useHistory();
    const {opened, setOpened, anchor, onExit} = useContext(UserMenuContext);
    const classes = useStyles();

    const exitHandler = () => {
        onExit();
        history.push('/');
    }

    return (
        <Popover
            open={opened}
            anchorEl={anchor}
            onClose={() => setOpened(false)}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
        >
            <div className={classes.content}>
                { user.email }
                <div className={classes.btns}>
                    <Button size="small" color={'secondary'} disableElevation variant="contained" onClick={onExit}>Выйти</Button>
                </div>
            </div>
        </Popover>
    );
};

export default AppUserMenu;