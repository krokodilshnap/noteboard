import React, {useCallback, useContext, useEffect, useState} from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import {useStyles} from './styles';
import {debounce} from '../../helpers/debounce';
import axios from 'axios';
import {NoteContext} from '../../contexts/notes/noteContext';
import {AuthContext} from '../../contexts/auth/authContext';

const AppSearch = props => {
    const classes = useStyles();
    const [value, setValue] = useState('');
    const {user} = useContext(AuthContext);
    const {setFormattedNotes} = useContext(NoteContext);

    const searchHandler = (value) => {
        setValue(value);
    };

    const search = useCallback(debounce((value, user) => onSearch(value, user), 1000), []);

    useEffect(() => {
        search(value, user);
    }, [value, search, user]);

    const onSearch = async (value, user) => {
        const response = await axios.get(`${process.env.REACT_APP_API_URL}/notes.json?orderBy="theme"&startAt="${value}"`);
        const notes = response.data;
        setFormattedNotes(notes);
    };

    return (
        <div className={classes.search}>
            <div className={classes.searchIcon}>
                <SearchIcon/>
            </div>
            <InputBase
                placeholder="Поиск..."
                onChange={(event) => searchHandler(event.target.value)}
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                value={value}
                inputProps={{'aria-label': 'search'}}
            />
        </div>
    );
};

export default AppSearch;