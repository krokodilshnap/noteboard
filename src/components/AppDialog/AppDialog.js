import React, {useContext, useState} from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import {DialogContext} from '../../contexts/dialog/dialogContext';
import {DialogActions, useStyles} from './styles';
import axios from 'axios';
import {AuthContext} from '../../contexts/auth/authContext';
import {validateAllFields} from '../../helpers/validation';
import {SnackbarContext} from '../../contexts/snackbar/snackbarContext';


const AppDialog = props => {
    const classes = useStyles();
    const {opened, setOpened} = useContext(DialogContext);
    const {loginUser} = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [valid, setValid] = useState(true);
    const snackbar = useContext(SnackbarContext);

    const sign = async (method) => {
        const data = {
            email,
            password,
            returnSecureToken: true
        };
        const signUrl = `https://identitytoolkit.googleapis.com/v1/accounts:${method}?key=AIzaSyB9FAY5aKt2eh0qoFeqWh4yyjmj9_pB2Zs`;

        setValid(validateAllFields(email, password));

        if (!valid) return;

        try {
            const response = await axios.post(signUrl, data);
            if (method === 'signInWithPassword') {
                localSignIn(response);
                closeDialog();
            } else if (method === 'signUp') {
                clearFields();
                snackbar.showWithMessage('Пользователь успешно зарегистрирован, можете войти в систему');
            }
        } catch (e) {
            setValid(false);

            switch (e.response.data.error.message) {
                case 'EMAIL_EXISTS':
                    snackbar.showWithMessage('Такой пользователь уже существует');
                    break;
                case 'EMAIL_NOT_FOUND':
                    snackbar.showWithMessage('Такого пользователя не существует');
                    break;
                default:
                    throw new Error(e.response.data.error.message);
            }
        }

    };

    const clearFields = () => {
        setPassword('');
        setEmail('');
    };

    const localSignIn = response => {
        const {localId, idToken, expiresIn, email} = response.data;
        const expiresDate = new Date(Date.now() + (expiresIn * 1000)).toString();

        loginUser({
            localId, idToken, expiresDate, email
        });
    };

    const closeDialog = () => {
        clearFields();
        setOpened(false);
    };

    return (
        <Dialog open={opened} onClose={closeDialog} fullWidth maxWidth="xs"
                aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Войти</DialogTitle>
            <DialogContent>
                <div>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="E-mail"
                        error={!valid}
                        size="small"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                        variant="outlined"
                    />
                </div>
                <div>
                    <TextField
                        required
                        margin="normal"
                        fullWidth
                        type="password"
                        id="password"
                        value={password}
                        label="Пароль"
                        error={!valid}
                        helperText="Пароль должен быть не менее 6 символов"
                        onChange={(event) => setPassword(event.target.value)}
                        size="small"
                        variant="outlined"
                    />
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => sign('signUp')} color="default" className={classes.registerBtn}>
                    Регистрация
                </Button>
                <Button onClick={closeDialog} color="default">
                    Отмена
                </Button>
                <Button onClick={() => sign('signInWithPassword')} color="primary">
                    Вход
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AppDialog;