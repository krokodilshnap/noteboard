import {makeStyles, withStyles} from '@material-ui/core/styles';
import MuiDialogActions from '@material-ui/core/DialogActions';

export const useStyles = makeStyles(theme => ({
    inputBase: {
        width: '100%',
        marginBottom: '10px'
    },
    registerBtn: {
        marginRight: 'auto'
    },
}));

export const DialogActions = withStyles((theme) => ({
    root: {
        margin: '30px 0 0 0',
        padding: '8px 16px',
    },
}))(MuiDialogActions);