import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    noteActions: {
        justifyContent: 'space-between'
    }
});