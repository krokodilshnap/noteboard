import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import {NavLink} from 'react-router-dom';
import {useStyles} from './styles';

const Note = props => {
    const classes = useStyles();


    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {props.title}
                </Typography>
                <Typography variant="body2" component="p">
                   {props.text}
                </Typography>
            </CardContent>
            <CardActions className={classes.noteActions}>
                <Button component={NavLink} to={'/' + props.id} size="small">Открыть</Button>
                <Button onClick={(event) => props.onDelete(event.currentTarget, props.id)} size="small" color={'secondary'}>Удалить</Button>
            </CardActions>
        </Card>
    );
};

export default Note;