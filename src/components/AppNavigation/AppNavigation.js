import React, {useContext} from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import RateReviewRoundedIcon from '@material-ui/icons/RateReviewRounded';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import {useStyles} from './styles';
import AppSearch from '../AppSearch/AppSearch';
import {NavLink} from 'react-router-dom';
import {DialogContext} from '../../contexts/dialog/dialogContext';
import {AuthContext} from '../../contexts/auth/authContext';
import {UserAvatar} from './styles';
import {useLocation} from 'react-router-dom';
import {UserMenuContext} from '../../contexts/userMenu/userMenuContext';

const AppNavigation = props => {
    const classes = useStyles();
    const dialog = useContext(DialogContext);
    const {user} = useContext(AuthContext);
    const location = useLocation();
    const userMenu = useContext(UserMenuContext);

    const openLoginForm = () => {
        dialog.setOpened(true);
    };

    return (
        <AppBar position="sticky" className={classes.appBar}>
            <Toolbar>
                <NavLink to="/" exact className={classes.mainLogoLink}>
                    <RateReviewRoundedIcon className={classes.mainLogo}/>
                    <Typography variant="h6" noWrap>
                        Noteboard
                    </Typography>
                </NavLink>
                {location.pathname === '/' && <AppSearch/>}
                <div className={classes.navControls}>
                    { !user
                        ? <Button variant="outlined" color="primary" className={classes.loginBtn} onClick={openLoginForm}>Войти</Button>
                        : <React.Fragment>
                            <Button color="primary" component={NavLink} to="/create-note" variant="contained">
                                Новая заметка
                            </Button>
                            <UserAvatar style={{cursor: 'pointer'}} variant="rounded" onClick={(event) => userMenu.show(event.currentTarget)}>{user.email.charAt(0).toUpperCase()}</UserAvatar>
                        </React.Fragment> }
                </div>

            </Toolbar>
        </AppBar>
    );
};

export default AppNavigation;