import {makeStyles, withStyles} from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import {Avatar} from '@material-ui/core';
import {orange} from '@material-ui/core/colors';

export const UserAvatar = withStyles({
    root: {
        width: 36,
        height: 36,
        marginLeft: 20,
        backgroundColor: orange[700]
    }
})(Avatar);

export const useStyles = makeStyles((theme) => ({
    appBar: {
        backgroundColor: 'inherit',
        boxShadow: 'none',
        marginTop: 20,
        color: indigo[800]
    },
    mainLogo: {
        marginRight: '10px',
        color: indigo[800]
    },
    mainLogoLink: {
        display: 'flex',
        alignItems: 'center'
    },
    loginBtn: {
        marginLeft: '20px'
    },
    navControls: {
        marginLeft: 'auto',
        display: 'flex',
        alignItems: 'center'
    }
}));