import React, {useContext} from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import {SnackbarContext} from '../../contexts/snackbar/snackbarContext';

const AppSnackbar = props => {
    const {opened, setOpened, message, status} = useContext(SnackbarContext);

    return (
        <Snackbar open={opened} autoHideDuration={6000} onClose={() => setOpened(false)} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
            <Alert onClose={() => setOpened(false)} severity={status}>
                {message}
            </Alert>
        </Snackbar>
    );
};

export default AppSnackbar;