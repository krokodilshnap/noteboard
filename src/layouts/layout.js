import React, {useContext, useEffect} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import AppNavigation from '../components/AppNavigation/AppNavigation';
import {useStyles} from './styles';
import AppDialog from '../components/AppDialog/AppDialog';
import {AuthContext} from '../contexts/auth/authContext';
import AppSnackbar from '../components/Snackbar/AppSnackbar';

function useAutoLogin() {
    const {loginUser, logout} = useContext(AuthContext);

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem('user'));

        if (user && new Date() < new Date(user.expiresDate)) {
            loginUser(user);
        } else {
            logout();

        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
};



export default function Layout({children}) {
    const classes = useStyles();
    useAutoLogin();

    return (
        <div className={classes.root}>
            <Container maxWidth={'md'}>
                <CssBaseline/>
                <AppNavigation/>
                <main className={classes.content}>
                    {children}
                </main>
                <AppDialog />
                <AppSnackbar/>
            </Container>
        </div>
    );
}
