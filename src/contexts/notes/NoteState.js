import React, {useState} from 'react';
import {NoteContext} from './noteContext';
import axios from 'axios';

const NoteState = ({children}) => {
    const [notes, setNotes] = useState(null);

    const deleteNote = async (id) => {
        await axios.delete(`${process.env.REACT_APP_API_URL}/notes/${id}.json`);
        setNotes(notes.filter(note => note.id !== id));
    };

    const setFormattedNotes = notes => {
        const previewLength = 50;
        const result = Object.keys(notes).map(key => {
            return {
                ...notes[key],
                id: key,
                note: notes[key].note.substr(0, previewLength) + (notes[key].note.length > previewLength ? '...' : '')
            }
        });
        setNotes(result);
    };

    return (
        <NoteContext.Provider value={{
            notes, setNotes, deleteNote, setFormattedNotes
        }}>
            {children}
        </NoteContext.Provider>
    );
};

export default NoteState;