import React, {useState} from 'react';
import {SnackbarContext} from './snackbarContext';

const SnackbarState = ({children}) => {
    const [opened, setOpened] = useState(false);
    const [message, setMessage] = useState('Ошибка');
    const [status, setStatus] = useState('error');

    const showWithMessage = (message, status) => {
        setMessage(message);
        setOpened(true);
        setStatus(status);
    };

    return (
        <SnackbarContext.Provider value={{
            opened, message, showWithMessage, setOpened,
            status
        }}>
            {children}
        </SnackbarContext.Provider>
    );
};

export default SnackbarState;