import React, {useContext, useState} from 'react';
import {UserMenuContext} from './userMenuContext';
import {AuthContext} from '../auth/authContext';

const UserMenuState = ({children}) => {
    const [opened, setOpened] = useState(false);
    const [anchor, setAnchor] = useState(null);
    const auth = useContext(AuthContext);

    const show = (anchor) => {
        setAnchor(anchor);
        setOpened(true);
    };

    const onExit = () => {
        auth.logout();
        setOpened(false);
    };

    return (
        <UserMenuContext.Provider value={{
            opened, setOpened,
            anchor, show, onExit
        }}>
            {children}
        </UserMenuContext.Provider>
    );
};

export default UserMenuState;