import React, {useState} from 'react';
import {ConfirmContext} from './confirmContext';

const ConfirmState = ({children}) => {
    const [opened, setOpened] = useState(false);
    const [isConfirm, setConfirm] = useState(false);
    const [anchor, setAnchor] = useState();

    const show = (anchor) => {
        setAnchor(anchor);
        setOpened(true);
        setConfirm(false);
    };

    const onConfirm = () => {
        setConfirm(true);
        setOpened(false);
    };

    const onDecline = () => {
        setConfirm(false);
        setOpened(false);
    };

    return (
        <ConfirmContext.Provider value={{
            opened, setOpened,
            anchor, isConfirm, setConfirm,
            show, onConfirm, onDecline
        }}>
            {children}
        </ConfirmContext.Provider>
    );
};

export default ConfirmState;