import React, {useState} from 'react';
import {DialogContext} from './dialogContext';

const DialogState = ({children}) => {
    const [opened, setOpened] = useState(false);

    return (
        <DialogContext.Provider value={{
            opened,
            setOpened
        }}>
            {children}
        </DialogContext.Provider>
    );
};

export default DialogState;