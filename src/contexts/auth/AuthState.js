import React, {useReducer} from 'react';
import {AuthContext} from './authContext';
import {authReducer} from './authReducer';
import {LOGIN, LOGOUT} from './types';

const AuthState = ({children}) => {
    const initialState = {
        user: ''
    };

    const [state, dispatch] = useReducer(authReducer, initialState);

    const loginUser = (data) => {

        dispatch({
            type: LOGIN,
            payload: data
        });

        localStorage.setItem('user', JSON.stringify(data));
    };

    const logout = () => {
        dispatch({type: LOGOUT});
        localStorage.removeItem('user');
    };

    const {user} = state;

    return (
        <AuthContext.Provider value={{
            user, loginUser, logout
        }}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthState;