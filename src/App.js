import React from 'react';
import './App.css';
import Layout from './layouts/layout';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from './pages/Home/Home';
import NotePage from './pages/NotePage/NotePage';
import DialogState from './contexts/dialog/DialogState';
import CreateNote from './pages/CreateNote/CreateNote';
import AuthState from './contexts/auth/AuthState';
import SnackbarState from './contexts/snackbar/SnackbarState';
import NoteState from './contexts/notes/NoteState';
import ConfirmState from './contexts/confirm/ConfirmState';
import UserMenuState from './contexts/userMenu/UserMenuState';

function App() {

    return (
        <ConfirmState>
            <NoteState>
                <SnackbarState>
                    <AuthState>
                        <UserMenuState>
                            <DialogState>
                                <BrowserRouter>
                                    <Layout>
                                        <Switch>
                                            <Route path="/" exact component={Home}/>
                                            <Route path="/create-note" exact component={CreateNote}/>
                                            <Route path="/:id" exact component={NotePage}/>
                                            <Route path="/edit/:id" component={CreateNote}/>
                                        </Switch>
                                    </Layout>
                                </BrowserRouter>
                            </DialogState>
                        </UserMenuState>
                    </AuthState>
                </SnackbarState>
            </NoteState>
        </ConfirmState>
    );
}

export default App;
